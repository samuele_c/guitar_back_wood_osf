## #####################################################
## ## Regression overall~overall2
## #####################################################

t1 = Sys.time()
source("../DBDA2E-utilities_SC.R") #postprocessing utilities based on code of Kruschke
source("../DBDA2E-utilities_DF.R") #postprocessing utilities based on code of Kruschke
source("../Jags-Ymet-Xmet-Mrobust-Subj.R")
currDir = getwd()
setwd("../")
source("global_parameters.R")
setwd(currDir)
source("../utils.R")
#load libraries for multi-core parallel processing
library(foreach)
library(doParallel)
library(RColorBrewer)
library(dplyr)
registerDoParallel(cores=8)

##
updateMCMCSampling = TRUE


##directory where to save the results
resDir = "../../results/blinded_correlation_rep1_rep2/"
dir.create(resDir, recursive=T, showWarnings=F)
dir.create(paste0(resDir, "MCMC_diagnostics"), recursive=T, showWarnings=F)
dir.create(paste0(resDir, "figures_data"), recursive=T, showWarnings=F)
## set parameters for MCMC sampling
numSavedStepsCorrR1R2 = 100000
numThinStepsCorrR1R2 = 15

datRep12 = read.table("../../derived_datasets/blinded_ratings_rep1_rep2.csv", header=T)

if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC_overall.RDS")) == FALSE){
    chainListRegOverallZ12 = genMCMCReg1VarSubjRobustIndCoeff(data=datRep12, xName="overallZ2", 
        yName="overallZ", sName="subject",
        numSavedSteps=numSavedStepsCorrR1R2, thinSteps=numThinStepsCorrR1R2)
    saveRDS(chainListRegOverallZ12, file=paste0(resDir, "MCMC_overall.RDS"), compress="xz")
} else {
    chainListRegOverallZ12 = readRDS(paste0(resDir, "MCMC_overall.RDS"))
}

mcmcMat = as.matrix(chainListRegOverallZ12, chains=T)
parameterNames = varnames(chainListRegOverallZ12) # get all parameter names
parToPlot = c("zbeta0mu","zbeta1mu", "beta0mu","beta1mu","nu","sigma","beta0[1]","beta1[1]", "nuzbeta0sigma", "nuzbeta1sigma")
foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/overall_", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=chainListRegOverallZ12, parName=parName)
    dev.off()
}
    
summaryInfoRegOverallZ12 = summary(chainListRegOverallZ12)
    
pdf(file=paste0(resDir, "overall_posterior.pdf"), width=10, height=6)
plot.Reg1VarSubjRobustIndCoeff(chainListRegOverallZ12, data=datRep12, xName="overallZ2", yName="overallZ", sName="subject",
                         compValBeta1=0.0, ropeBeta1=c(-0.1,0.1) ,
                         pairsPlot=FALSE, showCurve=FALSE)
dev.off()
    
subjs = unique(datRep12$subject)
vals = numeric()
for (i in 1:length(subjs)){
    thisDat = datRep12[datRep12$subject == subjs[i],]
    thisCor = cor.test(thisDat$overall, thisDat$overall2)
    vals = c(vals, thisCor$estimate)
}

thisPal = brewer.pal(7, "Set2")[1:6]
thisPal = rep("black", 6) 
theseLims = c(-2,2)
uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "overall_posterior_individual.pdf"))
par(mfrow=c(4,2))
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plotPost(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")], main=paste("Intercept", thisSubj))
    plotPost(mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")], main=paste("Slope", thisSubj))
}
dev.off()

nSampPostCheck = 500
nChainSamp = dim(mcmcMat)[1]
idx = sample(1:nChainSamp, nSampPostCheck)
pdf(paste0(resDir, "overall_cor_r1_r2_samp.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4.1,1,0.9))
xseq = seq(-2.2,2.2,0.1)
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("green", alpha=0.5),
            border=NA)
    for (indn in idx){
        abline(coef=c(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")][indn], mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")][indn]), col=add.alpha("gray", 0.1), lwd=1)
    }
    g = lm(datRep12[datRep12$subject== thisSubj, "overallZ2"]~datRep12[datRep12$subject== thisSubj, "overallZ"])
    abline(g, col="skyblue", lwd=2)
    abline(coef=c(summaryInfoRegOverallZ12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegOverallZ12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)

    points(datRep12[datRep12$subject== thisSubj, "overallZ"], datRep12[datRep12$subject== thisSubj, "overallZ2"], pch=16, col=thisPal, cex=1.5)

    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}

title(xlab=expression(paste("Overall Sound Rating Sess. #1 (", italic("z"), " Score)")), ylab=expression(paste("Overall Sound Rating Sess. #2 (", italic("z"), " Score)")), outer=T)
dev.off()


pdf(paste0(resDir, "overall_cor_r1_r2.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4.1,1,0.9))
xseq = seq(-2.5,2.5,0.1)
df_cor12_lims_overall = data.frame()
df_cor12_lims2_overall = data.frame()
df_cor12_coeffs_overall = data.frame()
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("gray", alpha=0.5),
            border=NA)
    g = lm(datRep12[datRep12$subject== thisSubj, "overallZ2"]~datRep12[datRep12$subject== thisSubj, "overallZ"])
    abline(g, col="skyblue", lwd=2)
    abline(coef=c(summaryInfoRegOverallZ12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegOverallZ12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)
    points(datRep12[datRep12$subject== thisSubj, "overallZ"], datRep12[datRep12$subject== thisSubj, "overallZ2"], pch=16, col=thisPal, cex=1.5)

    df_cor12_coeffs_overall = rbind(df_cor12_coeffs_overall,
        data.frame(subject=thisSubj, interceptLinear = coefficients(g)[[1]], slopeLinear=coefficients(g)[[2]], interceptMCMC=summaryInfoRegOverallZ12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], slopeMCMC = summaryInfoRegOverallZ12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]))
    this_df_cor12_lims = data.frame(xseq=xseq, ycredlo=ycredlo, ycredhi=ycredhi)
    this_df_cor12_lims2 = data.frame(x=c(xseq, rev(xseq)), y=c(ycredlo, rev(ycredhi)))
    this_df_cor12_lims$subject = thisSubj
    this_df_cor12_lims2$subject = thisSubj
    df_cor12_lims_overall = rbind(df_cor12_lims_overall, this_df_cor12_lims)
    df_cor12_lims2_overall = rbind(df_cor12_lims2_overall, this_df_cor12_lims2)
    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}

title(xlab=expression(paste("Overall Sound Rating Sess. #1 (", italic("z"), " Score)")), ylab=expression(paste("Overall Sound Rating Sess. #2 (", italic("z"), " Score)")), outer=T)
dev.off()
write.table(df_cor12_lims_overall, paste0(resDir, "figures_data/", "df_cor12_lims_overall.csv"), col.names=T, row.names=F)
write.table(df_cor12_lims2_overall, paste0(resDir, "figures_data/", "df_cor12_lims2_overall.csv"), col.names=T, row.names=F)
write.table(df_cor12_coeffs_overall, paste0(resDir, "figures_data/", "df_cor12_coeffs_overall.csv"), col.names=T, row.names=F)

#####################################################
## Regression playabilityZ~playabilityZ2
#####################################################

if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC_playability.RDS")) == FALSE){
chainListRegPlayabilityZ12 = genMCMCReg1VarSubjRobustIndCoeff(data=datRep12, xName="playabilityZ2", 
    yName="playabilityZ", sName="subject",
    numSavedSteps=numSavedStepsCorrR1R2, thinSteps=numThinStepsCorrR1R2)
    saveRDS(chainListRegPlayabilityZ12, file=paste0(resDir, "MCMC_playability.RDS"), compress="xz")
} else {
    chainListRegPlayabilityZ12 = readRDS(paste0(resDir, "MCMC_playability.RDS"))
}


mcmcMat = as.matrix(chainListRegPlayabilityZ12, chains=T)
parameterNames = varnames(chainListRegPlayabilityZ12) # get all parameter names
parToPlot = c("zbeta0mu","zbeta1mu", "beta0mu","beta1mu","nu","sigma","beta0[1]","beta1[1]", "nuzbeta0sigma", "nuzbeta1sigma")
foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/playability_", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=chainListRegPlayabilityZ12, parName=parName)
    dev.off()
}
    
summaryInfoRegPlayabilityZ12 = summary(chainListRegPlayabilityZ12)
    
pdf(file=paste0(resDir, "playability_posterior.pdf"), width=10, height=6)
plot.Reg1VarSubjRobustIndCoeff(chainListRegPlayabilityZ12, data=datRep12, xName="playabilityZ2", yName="playabilityZ", sName="subject",
                         compValBeta1=0.0, ropeBeta1=c(-0.1,0.1) ,
                         pairsPlot=FALSE, showCurve=FALSE)
dev.off()
    
subjs = unique(datRep12$subject)
vals = numeric()
for (i in 1:length(subjs)){
    thisDat = datRep12[datRep12$subject == subjs[i],]
    thisCor = cor.test(thisDat$playability, thisDat$playability2)
    vals = c(vals, thisCor$estimate)
}

thisPal = brewer.pal(7, "Set2")[1:6]#[c(1:5,7)]
thisPal = rep("black", 6) 
theseLims = c(-2,2)
uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "playability_posterior_individual.pdf"))
par(mfrow=c(4,2))
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plotPost(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")], main=paste("Intercept", thisSubj))
    plotPost(mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")], main=paste("Slope", thisSubj))
}
dev.off()


pdf(paste0(resDir, "playability_cor_r1_r2.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4,1,1))
xseq = seq(-2.2,2.2,0.1)
df_cor12_lims_playability = data.frame()
df_cor12_coeffs_playability = data.frame()
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("gray", alpha=0.5),
            border=NA)
    g = lm(datRep12[datRep12$subject== thisSubj, "playabilityZ2"]~datRep12[datRep12$subject== thisSubj, "playabilityZ"])
    try(abline(g, col="skyblue", lwd=2))
    abline(coef=c(summaryInfoRegPlayabilityZ12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegPlayabilityZ12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)
    points(datRep12[datRep12$subject== thisSubj, "playabilityZ"], datRep12[datRep12$subject== thisSubj, "playabilityZ2"], pch=16, col=thisPal, cex=1.5)

    df_cor12_coeffs_playability = rbind(df_cor12_coeffs_playability,
        data.frame(subject=thisSubj, interceptLinear = coefficients(g)[[1]], slopeLinear=coefficients(g)[[1]], interceptMCMC=summaryInfoRegPlayabilityZ12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], slopeMCMC = summaryInfoRegPlayabilityZ12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]))
    this_df_cor12_lims = data.frame(xseq=xseq, ycredlo=ycredlo, ycredhi=ycredhi)
    this_df_cor12_lims$subject = thisSubj
    df_cor12_lims_playability = rbind(df_cor12_lims_playability, this_df_cor12_lims)
    
    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}
title(xlab="Playability rating sess. #1 (z -score)", ylab="playability rating sess. #2 (z score)", outer=T)
title(xlab=expression(paste("Playability rating sess. #1 (", italic("z"), " score)")), ylab=expression(paste("Playability rating sess. #2 (", italic("z"), " score)")), outer=T)
dev.off()
write.table(df_cor12_lims_playability, paste0(resDir, "figures_data/", "df_cor12_lims_playability.csv"), col.names=T, row.names=F)
write.table(df_cor12_coeffs_playability, paste0(resDir, "figures_data/", "df_cor12_coeffs_playability.csv"), col.names=T, row.names=F)



## ###############
## Factor 1
## ###############

if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC_F1.RDS")) == FALSE){
    chainListRegF1Z12 = genMCMCReg1VarSubjRobustIndCoeff(data=datRep12, xName="F1Z2", 
        yName="F1Z", sName="subject",
        numSavedSteps=numSavedStepsCorrR1R2, thinSteps=numThinStepsCorrR1R2)
    saveRDS(chainListRegF1Z12, file=paste0(resDir, "MCMC_F1.RDS"), compress="xz")
} else {
    chainListRegF1Z12 = readRDS(paste0(resDir, "MCMC_F1.RDS"))
}

mcmcMat = as.matrix(chainListRegF1Z12, chains=T)
parameterNames = varnames(chainListRegF1Z12) # get all parameter names
parToPlot = c("zbeta0mu","zbeta1mu", "beta0mu","beta1mu","nu","sigma","beta0[1]","beta1[1]", "nuzbeta0sigma", "nuzbeta1sigma")
foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/F1_", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=chainListRegF1Z12, parName=parName)
    dev.off()
}
    
summaryInfoRegF1Z12 = summary(chainListRegF1Z12)
    
pdf(file=paste0(resDir, "F1_Posterior.pdf"), width=10, height=6)
plot.Reg1VarSubjRobustIndCoeff(chainListRegF1Z12, data=datRep12, xName="F1Z2", yName="F1Z", sName="subject",
                         compValBeta1=0.0, ropeBeta1=c(-0.1,0.1) ,
                         pairsPlot=FALSE, showCurve=FALSE)
dev.off()
    
subjs = unique(datRep12$subject)
vals = numeric()
for (i in 1:length(subjs)){
    thisDat = datRep12[datRep12$subject == subjs[i],]
    thisCor = cor.test(thisDat$F1, thisDat$F12)
    vals = c(vals, thisCor$estimate)
}

thisPal = brewer.pal(7, "Set2")[1:6]#[c(1:5,7)]
thisPal = rep("black", 6) 
theseLims = c(-2,2)
uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F1_posterior_individual.pdf"))
par(mfrow=c(4,2))
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plotPost(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")], main=paste("Intercept", thisSubj))
    plotPost(mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")], main=paste("Slope", thisSubj))
}
dev.off()


uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F1_cor_r1_r2.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4,1,1))
xseq = seq(-2.2,2.2,0.1)
df_cor12_lims_F1 = data.frame()
df_cor12_coeffs_F1 = data.frame()
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("gray", alpha=0.5),
            border=NA)
    g = lm(datRep12[datRep12$subject== thisSubj, "F1Z2"]~datRep12[datRep12$subject== thisSubj, "F1Z"])
    try(abline(g, col="skyblue", lwd=2))
    abline(coef=c(summaryInfoRegF1Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegF1Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)
    points(datRep12[datRep12$subject== thisSubj, "F1Z"], datRep12[datRep12$subject== thisSubj, "F1Z2"], pch=16, col=thisPal, cex=1.5)

    df_cor12_coeffs_F1 = rbind(df_cor12_coeffs_F1,
        data.frame(subject=thisSubj, interceptLinear = coefficients(g)[[1]], slopeLinear=coefficients(g)[[1]], interceptMCMC=summaryInfoRegF1Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], slopeMCMC = summaryInfoRegF1Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]))
    this_df_cor12_lims = data.frame(xseq=xseq, ycredlo=ycredlo, ycredhi=ycredhi)
    this_df_cor12_lims$subject = thisSubj
    df_cor12_lims_F1 = rbind(df_cor12_lims_F1, this_df_cor12_lims)
    
    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}

title(xlab=expression(paste("F1 rating sess. #1 (", italic("z"), " score)")), ylab=expression(paste("F1 rating sess. #2 (", italic("z"), " score)")), outer=T)
dev.off()
write.table(df_cor12_lims_F1, paste0(resDir, "figures_data/", "df_cor12_lims_F1.csv"), col.names=T, row.names=F)
write.table(df_cor12_coeffs_F1, paste0(resDir, "figures_data/", "df_cor12_coeffs_F1.csv"), col.names=T, row.names=F)

## ###############
## Factor 2
## ###############
if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC_F2.RDS")) == FALSE){
    chainListRegF2Z12 = genMCMCReg1VarSubjRobustIndCoeff(data=datRep12, xName="F2Z2", 
        yName="F2Z", sName="subject",
        numSavedSteps=numSavedStepsCorrR1R2, thinSteps=numThinStepsCorrR1R2)
    saveRDS(chainListRegF2Z12, file=paste0(resDir, "MCMC_F2.RDS"), compress="xz")
} else {
    chainListRegF2Z12 = readRDS(paste0(resDir, "MCMC_F2.RDS"))
}

mcmcMat = as.matrix(chainListRegF2Z12, chains=T)
parameterNames = varnames(chainListRegF2Z12) # get all parameter names
parToPlot = c("zbeta0mu","zbeta1mu", "beta0mu","beta1mu","nu","sigma","beta0[1]","beta1[1]", "nuzbeta0sigma", "nuzbeta1sigma")
foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=chainListRegF2Z12, parName=parName)
    dev.off()
}
    
summaryInfoRegF2Z12 = summary(chainListRegF2Z12)
    
pdf(file=paste0(resDir, "F2_Posterior.pdf"), width=10, height=6)
plot.Reg1VarSubjRobustIndCoeff(chainListRegF2Z12, data=datRep12, xName="F2Z2", yName="F2Z", sName="subject",
                         compValBeta1=0.0, ropeBeta1=c(-0.1,0.1) ,
                         pairsPlot=FALSE, showCurve=FALSE)
dev.off()
    
subjs = unique(datRep12$subject)
vals = numeric()
for (i in 1:length(subjs)){
    thisDat = datRep12[datRep12$subject == subjs[i],]
    thisCor = cor.test(thisDat$F2, thisDat$F22)
    vals = c(vals, thisCor$estimate)
}

thisPal = brewer.pal(7, "Set2")[1:6]#[c(1:5,7)]
thisPal = rep("black", 6) 
theseLims = c(-2,2)
uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F2_posterior_individual.pdf"))
par(mfrow=c(4,2))
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plotPost(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")], main=paste("Intercept", thisSubj))
    plotPost(mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")], main=paste("Slope", thisSubj))
}
dev.off()


uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F2_cor_r1_r2.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4,1,1))
xseq = seq(-2.2,2.2,0.1)
df_cor12_lims_F2 = data.frame()
df_cor12_coeffs_F2 = data.frame()
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("gray", alpha=0.5),
            border=NA)
    g = lm(datRep12[datRep12$subject== thisSubj, "F2Z2"]~datRep12[datRep12$subject== thisSubj, "F2Z"])
    try(abline(g, col="skyblue", lwd=2))
    abline(coef=c(summaryInfoRegF2Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegF2Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)
    points(datRep12[datRep12$subject== thisSubj, "F2Z"], datRep12[datRep12$subject== thisSubj, "F2Z2"], pch=16, col=thisPal, cex=1.5)

    df_cor12_coeffs_F2 = rbind(df_cor12_coeffs_F2,
        data.frame(subject=thisSubj, interceptLinear = coefficients(g)[[1]], slopeLinear=coefficients(g)[[1]], interceptMCMC=summaryInfoRegF2Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], slopeMCMC = summaryInfoRegF2Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]))
    this_df_cor12_lims = data.frame(xseq=xseq, ycredlo=ycredlo, ycredhi=ycredhi)
    this_df_cor12_lims$subject = thisSubj
    df_cor12_lims_F2 = rbind(df_cor12_lims_F2, this_df_cor12_lims)
    
    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}

title(xlab=expression(paste("F2 rating sess. #1 (", italic("z"), " score)")), ylab=expression(paste("F2 rating sess. #2 (", italic("z"), " score)")), outer=T)
dev.off()
write.table(df_cor12_lims_F2, paste0(resDir, "figures_data/", "df_cor12_lims_F2.csv"), col.names=T, row.names=F)
write.table(df_cor12_coeffs_F2, paste0(resDir, "figures_data/", "df_cor12_coeffs_F2.csv"), col.names=T, row.names=F)


## ###############
## Factor 3
## ###############
if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC_F3.RDS")) == FALSE){
    chainListRegF3Z12 = genMCMCReg1VarSubjRobustIndCoeff(data=datRep12, xName="F3Z2", 
        yName="F3Z", sName="subject",
        numSavedSteps=numSavedStepsCorrR1R2, thinSteps=numThinStepsCorrR1R2)
    saveRDS(chainListRegF3Z12, file=paste0(resDir, "MCMC_F3.RDS"), compress="xz")
} else {
    chainListRegF3Z12 = readRDS(paste0(resDir, "MCMC_F3.RDS"))
}

mcmcMat = as.matrix(chainListRegF3Z12, chains=T)
parameterNames = varnames(chainListRegF3Z12) # get all parameter names
parToPlot = c("zbeta0mu","zbeta1mu", "beta0mu","beta1mu","nu","sigma","beta0[1]","beta1[1]", "nuzbeta0sigma", "nuzbeta1sigma")
foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/F3_", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=chainListRegF3Z12, parName=parName)
    dev.off()
}
    
summaryInfoRegF3Z12 = summary(chainListRegF3Z12)
    
pdf(file=paste0(resDir, "F3_Posterior.pdf"), width=10, height=6)
plot.Reg1VarSubjRobustIndCoeff(chainListRegF3Z12, data=datRep12, xName="F3Z2", yName="F3Z", sName="subject",
                         compValBeta1=0.0, ropeBeta1=c(-0.1,0.1) ,
                         pairsPlot=FALSE, showCurve=FALSE)
dev.off()
    
subjs = unique(datRep12$subject)
vals = numeric()
for (i in 1:length(subjs)){
    thisDat = datRep12[datRep12$subject == subjs[i],]
    thisCor = cor.test(thisDat$F3, thisDat$F32)
    vals = c(vals, thisCor$estimate)
}

thisPal = brewer.pal(7, "Set2")[1:6]#[c(1:5,7)]
thisPal = rep("black", 6) 
theseLims = c(-2,2)
uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F3_posterior_individual.pdf"))
par(mfrow=c(4,2))
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plotPost(mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")], main=paste("Intercept", thisSubj))
    plotPost(mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")], main=paste("Slope", thisSubj))
}
dev.off()


uniqueSubjID = unique(as.factor(datRep12$subject))
uniqueSubjNum = unique(as.numeric(as.factor(datRep12$subject)))

pdf(paste0(resDir, "F3_cor_r1_r2.pdf"), width=6.8, height=6.8)
par(mfrow=c(6,6), mar=c(0,0,0,0), oma=c(4,4,1,1))
xseq = seq(-2.2,2.2,0.1)
df_cor12_lims_F3 = data.frame()
df_cor12_coeffs_F3 = data.frame()
for (i in uniqueSubjNum){
    thisSubj = uniqueSubjID[uniqueSubjNum[i]]
    plot.new(); plot.window(xlim=theseLims, ylim=theseLims)
    ycredlo = numeric(length(xseq))
    ycredhi = numeric(length(xseq))
    for (x in 1:length(xseq)){
        y = mcmcMat[,paste0("beta0[",uniqueSubjNum[i],"]")]+mcmcMat[,paste0("beta1[",uniqueSubjNum[i],"]")]*xseq[x]
        yconflim = HDIofMCMC(y, credMass=0.95)
        ycredlo[x] = yconflim[1]
        ycredhi[x] = yconflim[2]
    }
    polygon(c(xseq, rev(xseq)), c(ycredlo, rev(ycredhi)), col=add.alpha("gray", alpha=0.5),
            border=NA)
    g = lm(datRep12[datRep12$subject== thisSubj, "F3Z2"]~datRep12[datRep12$subject== thisSubj, "F3Z"])
    try(abline(g, col="skyblue", lwd=2))
    abline(coef=c(summaryInfoRegF3Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], summaryInfoRegF3Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]), col="indianred", lwd=2)
    points(datRep12[datRep12$subject== thisSubj, "F3Z"], datRep12[datRep12$subject== thisSubj, "F3Z2"], pch=16, col=thisPal, cex=1.5)

    df_cor12_coeffs_F3 = rbind(df_cor12_coeffs_F3,
        data.frame(subject=thisSubj, interceptLinear = coefficients(g)[[1]], slopeLinear=coefficients(g)[[1]], interceptMCMC=summaryInfoRegF3Z12[paste0("beta0[",uniqueSubjNum[i],"]"), "Mode"], slopeMCMC = summaryInfoRegF3Z12[paste0("beta1[",uniqueSubjNum[i],"]"), "Mode"]))
    this_df_cor12_lims = data.frame(xseq=xseq, ycredlo=ycredlo, ycredhi=ycredhi)
    this_df_cor12_lims$subject = thisSubj
    df_cor12_lims_F3 = rbind(df_cor12_lims_F3, this_df_cor12_lims)
    
    box()
    text(-1.6,1.8, thisSubj)
    if (i %in% c(1,7,13,19,25,31)){
        axis(2, at=c(-1.5, 0, 1.5))
    }

    if (i>=29){
        axis(1, at=c(-1.5, 0, 1.5))
    }
}
title(xlab=expression(paste("F3 rating sess. #1 (", italic("z"), " score)")), ylab=expression(paste("F3 rating sess. #2 (", italic("z"), " score)")), outer=T)
dev.off()
write.table(df_cor12_lims_F3, paste0(resDir, "figures_data/", "df_cor12_lims_F3.csv"), col.names=T, row.names=F)
write.table(df_cor12_coeffs_F3, paste0(resDir, "figures_data/", "df_cor12_coeffs_F3.csv"), col.names=T, row.names=F)
