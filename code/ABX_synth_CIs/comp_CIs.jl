using CSV, DataFrames, DetectionTheory, Distributed, HDF5, Random, Statistics

@everywhere include("ideal_observer_ABX.jl")
@everywhere Random.seed!(20181126)

nBootSamp = 10000

tab = Dict{String, Any}()



t1 = time_ns()
dat1 = CSV.read("../../derived_datasets/ABX_synth_dprime.csv", delim=' ')


nVals = size(dat1)[1]
nTrials = 72
sampDistdprime = zeros(nVals, nBootSamp)
CIsLow = zeros(nVals)
CIsHigh = zeros(nVals)
ses = zeros(nVals)
frVec = zeros(nBootSamp)
hrVec = zeros(nBootSamp)
for vn = 1:nVals
    dp = dat1[:dprime][vn]
    #logBeta = dat1[:logBeta][vn]

    hrVec = zeros(nBootSamp)
    frVec = zeros(nBootSamp)
    dpVec = zeros(nBootSamp)
    #logBetaVec = zeros(nBootSamp)
    for i=1:nBootSamp
        res = pmap(summarizeABXDataLR, [dp], 0,
                   [nTrials])
        hrVec[i] = res[1][1]
        frVec[i] = res[1][2]
     
        if hrVec[i] > frVec[i]
            dpVec[i] = dprimeABX(hrVec[i], frVec[i], "IO")
        elseif hrVec[i] < frVec[i]
            dpVec[i] = -dprimeABX(frVec[i], hrVec[i], "IO")
        else
            dpVec[i] = 0
        end
    end

    CIsLow[vn] = quantile(dpVec, 0.025)
    CIsHigh[vn] = quantile(dpVec, 0.975)
    ses[vn] = std(dpVec)
    sampDistdprime[vn,:] = dpVec
end

dat1[:CILow] = CIsLow
dat1[:CIHigh] = CIsHigh
dat1[:se] = ses

CSV.write("../../derived_datasets/ABX_synth_dprime_CIs.csv", dat1, delim=' ')

t2 = time_ns()
println(float(t2-t1)*1e-9)
