using DataFrames, DocStringExtensions 

## possible trials:
## S1-S2-S1
## S2-S1-S2
## S1-S2-S2
## S2-S1-S1

"""
Generate ABX data using a likelihood ratio criterion.

$(SIGNATURES)

##### Parameters

* `d`: discriminability index of the observer.
* `logC': log of the likelihood ratio criterion (0 indicate an unbiased observer).
* `nTrials`: number of trials to simulate.

##### Returns

* `data`: a dataframe where each row corresponds to a trial, with columns:
     - `A`: the stimulus presented in the A interval, 0, or 1
     - `B`: the stimulus presented in the B interval, 0, or 1
     - `X`: the stimulus presented in the X interval, 0, or 1
     - `case`: whether the correct response is 'A', or 'B'
     - `respIO`: the response given by the IO observer (either A or B)
     - `respDiff`: the response given by the differencing observer (either A or B)

##### Examples

```julia
generateABXDataLR(1.5, 0, 150)
```
"""
function summarizeABXDataLR(d::Real=2, logC::Real=0, nTrials::Int=200)
    possibleSequences = [0 1 0;
                         1 0 1;
                         0 1 1;
                         1 0 0]

    dsign = sign(d)
    if d<0
        d = -d
    end
                     
    possibleSequencesD = possibleSequences.*d
    seqIdx = rand(collect(1:4), nTrials)
    thisSeq = possibleSequences[seqIdx,:]
    thisSeqD = possibleSequencesD[seqIdx,:]
    thisSeqD = thisSeqD+randn(nTrials,3)

    #IO observer
    lik = zeros(nTrials, 4)
    for i=1:4
        lik[:,i] = sum(-(thisSeqD.-possibleSequencesD[i,:]').^2/2, dims=2)
    end
    likA = sum(exp.(lik[:, 1:2]), dims=2)*(1/4)
    likB = sum(exp.(lik[:, 3:4]), dims=2)*(1/4)
    logLR = log.(likA)- log.(likB)
    
    ATrials = thisSeq[:,1] .== thisSeq[:,3]
    BTrials = thisSeq[:,2] .== thisSeq[:,3]

    respA = logLR .> logC
    PH = sum(respA[ATrials]) / sum(ATrials)
    PF = sum(respA[BTrials]) / sum(BTrials)

    nA = sum(ATrials)
    nB = sum(BTrials)
    nCorrA = sum(respA[ATrials])
    nIncorrB = sum(respA[BTrials])

    if nCorrA == nA
        HA = 1 - 1/(2*nA)
    elseif nCorrA == 0
        HA = 1/(2*nA)
    else
        HA = nCorrA / nA
    end

    if nIncorrB == nB
        FA = 1 - 1/(2*nB)
    elseif nIncorrB == 0
        FA = 1/(2*nB)
    else
        FA = nIncorrB / nB
    end
    
    if (dsign < 0)
        tmpHA = copy(FA)
        tmpFA = copy(HA)
        HA = tmpHA
        FA = tmpFA
    end
    
    return HA, FA
end


