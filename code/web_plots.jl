using ColorBrewer, CSV, DataFrames, DocStringExtensions, MATLAB, PlotlyJS, Statistics

resDir = "../results/web_plots/"

if isdir(resDir) == false
    mkpath(resDir)
end

guitarCodes = ["braz", "indi", "maho", "mapl", "sape", "waln"]
guitarLabelsShort = ["Braz. Rsw.", "Ind. Rsw.", "Mahogany", "Maple", "Sapele", "Walnut"]

cbPalette = ["#999999", "#E69F00", "#56B4E9", "#009E73", "#F0E442", "#0072B2", "#D55E00", "#CC79A7"]
singCol = palette("Pastel2", 3)[1]
twoCol = cbPalette[6:7]
threeCol = cbPalette[2:4]
threeCol = ["#d73027", "#f46d43", "#fdae61"] #palette("RdYlBu", 8)[[1,2,3]]
pTypeCol = palette("RdYlBu", 8)[[1,2,3]]
factorsCol = cbPalette[[4,6,8]]

"""
Substitute one or more strings with other strings in an array of strings.
"""
function subLabel(origLab, newLab, labs)
    newLabs = copy(labs)
    for nl=1:length(origLab)
        newLabs[newLabs .== origLab[nl]] = [newLab[nl] for i=1:length(newLabs[newLabs .== origLab[nl]])]
    end
    return newLabs
end

"""
Compute the standard error of the mean.

$(SIGNATURES)

##### Parameters

* `x`: Vector containing the values for which to compute the standard error.
* `removeNaN`: if true, NaN values will be removed from `x` before computing the standard error. Default is false.

##### Returns

`se`: The standard error of the mean.

##### Examples

```julia
SE([3, 8, 9])
SE([3, 8, NaN], removeNaN=false)
SE([3, 8, NaN], removeNaN=true)
```

"""
function SE(x::Union{AbstractVector{T}, AbstractVector{Union{T, Missing}}}; removeNaN::Bool=false) where {T<:Real}
    if removeNaN == true
         x = x[isnan(x) .== false]
    end
    n = length(x)
    out = std(x) / sqrt(n)
    return(out)
end


datRatings = CSV.read("../datasets/blinded_ratings.csv", delim=' ')
datRatingsSubjSummOverall = by(datRatings, [:subject, :guitar], df -> DataFrame(mean_subj_overall = mean(df[:overall]), mean_subj_playability=mean(df[:playability])))
datRatingsSumm = by(datRatingsSubjSummOverall, :guitar, df -> DataFrame(mean_overall=mean(df[:mean_subj_overall]), se_overall=SE(df[:mean_subj_overall]), mean_playability=mean(df[:mean_subj_playability]), se_playability=SE(df[:mean_subj_playability])))
datRatingsSumm[:guitarLabel] = subLabel(guitarCodes, guitarLabelsShort, datRatingsSumm[:guitar])

datRatingsSumm = sort(datRatingsSumm, (:guitarLabel))
e1 = Dict{Symbol,Any}(:type=>"data", :array=>datRatingsSumm[:se_overall])
trace1 = bar(;x=datRatingsSumm[:guitarLabel],
             y=datRatingsSumm[:mean_overall], marker_color=twoCol[1],
             name="Overall", error_y=e1)

e2 = Dict{Symbol,Any}(:type=>"data", :array=>datRatingsSumm[:se_playability])
trace2 = bar(;x=datRatingsSumm[:guitarLabel],
             y=datRatingsSumm[:mean_playability], marker_color=twoCol[2],
             name="Playability", error_y=e2)
data = [trace1, trace2]
layout = Layout(;barmode="group",
                yaxis_range=[1,5],
                yaxis_title="Rating",
                xaxis_title="Guitar Back/Side Wood",
                font_size=16,
                xaxis_tickangle=-45,
                hovermode="closest",
                margin_l = 50,
                xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_ticks="outside",
                yaxis_ticks="outside",
                width=600, height=600)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "overall_playability_barplot", ".html"), :remote)


datSummOverall = CSV.read("../results/blinded_overall/figures_data/df_within_contrasts.csv", delim=' ')
datSummOverall[:scale] = "Overall"
datSummPlayability = CSV.read("../results/blinded_playability/figures_data/df_within_contrasts.csv", delim=' ')
datSummPlayability[:scale] = "Playability"

datSummOverall = sort(datSummOverall, (:labelNice))
datSummPlayability = sort(datSummPlayability, (:labelNice))

e1 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false,
                      :array=>datSummOverall[:HDIhigh]-datSummOverall[:Mode],
                      :arrayminus=>abs.(datSummOverall[:HDIlow]-datSummOverall[:Mode]))
trace1 = scatter(;x=datSummOverall[:Mode], y=collect(1:15).+0.15,
                 mode="markers", name="Overall", error_x=e1, marker_size=10,
                 marker_color=twoCol[1])

e2 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false,
                      :array=>datSummPlayability[:HDIhigh]-datSummPlayability[:Mode],
                      :arrayminus=>abs.(datSummPlayability[:HDIlow]-datSummPlayability[:Mode]))
trace2 = scatter(;x=datSummPlayability[:Mode], y=collect(1:15).-0.15,
                 mode="markers", name="Playability", error_x=e2, marker_size=10,
                 marker_color=twoCol[2], marker_symbol="triangle-up")
tracevlinelo = scatter(;x=[-0.2, -0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")
tracevlinehi = scatter(;x=[0.2, 0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")                 

data = [trace1, trace2, tracevlinelo, tracevlinehi]
layout = Layout(;xaxis_range=[-1,1],
                yaxis_range=[0.5,15.5],
                yaxis_title="",
                xaxis_title="Rating Difference",
                font_size=16,
                xaxis_tickangle=0,#-45,
                hovermode="closest",
                margin_l = 170,
                #xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_showgrid=false,
                yaxis_tickvals=collect(1:15),
                xaxis_ticks="outside",
                yaxis_ticktext=datSummOverall[:labelNice],
                width=600, height=650)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "overall_playability_HDIs", ".html"), :remote)


## ###############################
## Overall Playability Unblinded
## ###############################
datSummOverallUnblinded = CSV.read("../results/unblinded_overall/figures_data/df_within_contrasts.csv", delim=' ')
datSummOverallUnblinded[:scale] = "Overall"
datSummPlayabilityUnblinded = CSV.read("../results/unblinded_playability/figures_data/df_within_contrasts.csv", delim=' ')
datSummPlayabilityUnblinded[:scale] = "Playability"

datSummOverallUnblinded = sort(datSummOverallUnblinded, (:labelNice))
datSummPlayabilityUnblinded = sort(datSummPlayabilityUnblinded, (:labelNice))

e1 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false,
                      :array=>datSummOverallUnblinded[:HDIhigh]-datSummOverallUnblinded[:Mode],
                      :arrayminus=>abs.(datSummOverallUnblinded[:HDIlow]-datSummOverallUnblinded[:Mode]))
trace1 = scatter(;x=datSummOverallUnblinded[:Mode], y=collect(1:15).+0.15,
                 mode="markers", name="Overall", error_x=e1, marker_size=10,
                 marker_color=twoCol[1])

e2 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false,
                      :array=>datSummPlayabilityUnblinded[:HDIhigh]-datSummPlayabilityUnblinded[:Mode],
                      :arrayminus=>abs.(datSummPlayabilityUnblinded[:HDIlow]-datSummPlayabilityUnblinded[:Mode]))
trace2 = scatter(;x=datSummPlayabilityUnblinded[:Mode], y=collect(1:15).-0.15,
                 mode="markers", name="Playability", error_x=e2, marker_size=10,
                 marker_color=twoCol[2], marker_symbol="triangle-up")
tracevlinelo = scatter(;x=[-0.2, -0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")
tracevlinehi = scatter(;x=[0.2, 0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")                 

data = [trace1, trace2, tracevlinelo, tracevlinehi]
layout = Layout(;xaxis_range=[-1,1],
                yaxis_range=[0.5,15.5],
                yaxis_title="",
                xaxis_title="Rating Difference",
                font_size=16,
                xaxis_tickangle=0,#-45,
                hovermode="closest",
                margin_l = 200,
                #xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_showgrid=false,
                yaxis_tickvals=collect(1:15),
                xaxis_ticks="outside",
                yaxis_ticktext=datSummOverall[:labelNice],
                width=600, height=650)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "overall_playability_unblinded_HDIs", ".html"), :remote)

## ###############################
## Overall Visual
## ###############################
datSummOverallVisual = CSV.read("../results/visual_overall/figures_data/df_within_contrasts.csv", delim=' ')
datSummOverallVisual[:scale] = "Overall"
datSummOverallVisual = sort(datSummOverallVisual, (:labelNice))

e1 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false, :color=>"black",
                      :array=>datSummOverallVisual[:HDIhigh]-datSummOverallVisual[:Mode],
                      :arrayminus=>abs.(datSummOverallVisual[:HDIlow]-datSummOverallVisual[:Mode]))
trace1 = scatter(;x=datSummOverallVisual[:Mode], y=collect(1:15),
                 mode="markers", name="Overall", error_x=e1, marker_size=10,
                 marker_color="black", showlegend=false)

tracevlinelo = scatter(;x=[-0.2, -0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")
tracevlinehi = scatter(;x=[0.2, 0.2], y=[0, 16], mode="line", line_color="gray", line_dash="dot", opacity=0.7, showlegend=false, name="LF", legendgroup="LF")                 

data = [trace1, tracevlinelo, tracevlinehi]
layout = Layout(;xaxis_range=[-1.5,1.5],
                yaxis_range=[0.5,15.5],
                yaxis_title="",
                xaxis_title="Rating Difference",
                font_size=16,
                xaxis_tickangle=0,#-45,
                hovermode="closest",
                margin_l = 200,
                #xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_showgrid=false,
                yaxis_tickvals=collect(1:15),
                xaxis_ticks="outside",
                yaxis_ticktext=datSummOverall[:labelNice],
                width=600, height=600)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "overall_visual_HDIs", ".html"), :remote)


## ################
## ABX
## ################

datABX = CSV.read("../derived_datasets/ABX_dprime.csv", delim=' ')
datABX[:cndPretty] = subLabel(["Rosewood-Sapele", "Rosewood-Walnut", "Walnut-Sapele"], ["Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Walnut vs Sapele"], datABX[:cnd])
datABXSumm = by(datABX, [:cndPretty, :type], df -> DataFrame(mean_dp=mean(df[:dprime]), se_dp=SE(df[:dprime])))

datABXSummPro = datABXSumm[datABXSumm[:type] .== "professional",:]
datABXSummSemi = datABXSumm[datABXSumm[:type] .== "semi-pro",:]
datABXSummAmat = datABXSumm[datABXSumm[:type] .== "amateur",:]

e1 = Dict{Symbol,Any}(:type=>"data", :array=>datABXSummPro[:se_dp])
trace1 = bar(;x=datABXSummPro[:cndPretty],
             y=datABXSummPro[:mean_dp], marker_color=threeCol[1],
             name="Professional", error_y=e1)

e2 = Dict{Symbol,Any}(:type=>"data", :array=>datABXSummSemi[:se_dp])
trace2 = bar(;x=datABXSummSemi[:cndPretty],
             y=datABXSummSemi[:mean_dp], marker_color=threeCol[2],
             name="Semi-pro", error_y=e2)

e3 = Dict{Symbol,Any}(:type=>"data", :array=>datABXSummAmat[:se_dp])
trace3 = bar(;x=datABXSummAmat[:cndPretty],
             y=datABXSummAmat[:mean_dp], marker_color=threeCol[3],
             name="Amateur", error_y=e3)


data = [trace1, trace2, trace3]
layout = Layout(;barmode="group",
                yaxis_range=[-1,4],
                yaxis_title="<i>d'</i>",
                xaxis_title="Guitar Pair",
                font_size=16,
                xaxis_tickangle=-45,
                hovermode="closest",
                margin_l = 50,
                #margin_b = 100,
                xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_ticks="outside",
                yaxis_ticks="outside",
                width=550, height=600)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "ABX_by_type_barplot", ".html"), :remote)

## ##########################
## ABX HDIs
## ##########################

df_summ_by_type_and_guitars_ABX = CSV.read("../results/ABX/figures_data/df_summ_by_type_and_guitars.csv", delim=' ')

ABXHDIPros = df_summ_by_type_and_guitars_ABX[df_summ_by_type_and_guitars_ABX.type .== "Professional",:]
ABXHDISemiPros = df_summ_by_type_and_guitars_ABX[df_summ_by_type_and_guitars_ABX.type .== "Semi-pro",:]
ABXHDIAmat = df_summ_by_type_and_guitars_ABX[df_summ_by_type_and_guitars_ABX.type .== "Amateur",:]

e1 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false, :color=>threeCol[1],
                      :array=>ABXHDIPros[:HDIhigh]-ABXHDIPros[:Mode],
                      :arrayminus=>abs.(ABXHDIPros[:HDIlow]-ABXHDIPros[:Mode]))
trace1 = scatter(;x=ABXHDIPros[:Mode], y=collect(1:3).+0.15,
                 mode="markers", name="Professional", error_x=e1, marker_size=10,
                 marker_color=threeCol[1], marker_symbol="circle")

e2 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false, :color=>threeCol[2],
                      :array=>ABXHDISemiPros[:HDIhigh]-ABXHDISemiPros[:Mode],
                      :arrayminus=>abs.(ABXHDISemiPros[:HDIlow]-ABXHDISemiPros[:Mode]))
trace2 = scatter(;x=ABXHDISemiPros[:Mode], y=collect(1:3).+0.0,
                 mode="markers", name="Semi-pro", error_x=e2, marker_size=10,
                 marker_color=threeCol[2], marker_symbol="triangle-up")

e3 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false, :color=>threeCol[3],
                      :array=>ABXHDIAmat[:HDIhigh]-ABXHDIAmat[:Mode],
                      :arrayminus=>abs.(ABXHDIAmat[:HDIlow]-ABXHDIAmat[:Mode]))
trace3 = scatter(;x=ABXHDIAmat[:Mode], y=collect(1:3).-0.15,
                 mode="markers", name="Amateur", error_x=e3, marker_size=10,
                 marker_color=threeCol[3], marker_symbol="square")

data = [trace1, trace2, trace3, tracevlinehi]
layout = Layout(;xaxis_range=[-1,4],
                yaxis_range=[0.5,3.5],
                yaxis_title="",
                xaxis_title="<i>d'</i>",
                font_size=16,
                xaxis_tickangle=0,#-45,
                hovermode="closest",
                margin_l = 170,
                #xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_showgrid=false,
                yaxis_tickvals=collect(1:3),
                xaxis_ticks="outside",
                yaxis_ticktext=["Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut"],
                width=600, height=500)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "ABX_HDIs", ".html"), :remote)

## ######################
## ABX synth. guitars
## ######################

datABXSynth = CSV.read("../derived_datasets/ABX_synth_dprime.csv", delim=' ')
datABXSynthSumm = by(datABXSynth, [:condition, :conditionLabels], df -> DataFrame(mean_dp=mean(df[:dprime]), se_dp=SE(df[:dprime])))

#idx = [1,2,5,3,4]
idx = [3, 5, 2, 4, 1]
e1 = Dict{Symbol,Any}(:type=>"data", :array=>datABXSynthSumm[:se_dp][idx])
trace1 = bar(;x=datABXSynthSumm[:conditionLabels][idx],
             y=datABXSynthSumm[:mean_dp][idx], marker_color=singCol,
             name="", error_y=e1)
data = [trace1]
layout = Layout(;barmode="group",
                yaxis_range=[-1,4],
                yaxis_title="<i>d'</i>",
                xaxis_title="Guitar Pair",
                font_size=16,
                xaxis_tickangle=-45,
                hovermode="closest",
                margin_b = 175,
                margin_l = 100,
                xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_ticks="outside",
                yaxis_ticks="outside",
                width=500, height=600)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "ABX_synth_barplot", ".html"), :remote)

## #########################
## ABX synth. guitars HDIs
## #########################

datABXSynthPosterior = CSV.read("../results/ABX_synth/figures_data/summDF.csv", delim=' ')

e1 = Dict{Symbol,Any}(:type=>"data", :symmetric=>false, :color=>"black",
                      :array=>datABXSynthPosterior[:HDIhigh]-datABXSynthPosterior[:Mode],
                      :arrayminus=>abs.(datABXSynthPosterior[:HDIlow]-datABXSynthPosterior[:Mode]))
trace1 = scatter(;x=datABXSynthPosterior[:Mode], y=collect(1:5).+0.15,
                 mode="markers", name="", error_x=e1, marker_size=10,
                 marker_color="black", marker_symbol="circle", showlegend=false)
data = [trace1, tracevlinehi]
layout = Layout(;xaxis_range=[-1,4],
                yaxis_range=[0.5,5.5],
                yaxis_title="",
                xaxis_title="<i>d'</i>",
                font_size=16,
                xaxis_tickangle=0,#-45,
                hovermode="closest",
                margin_l = 170,
                #xaxis_zeroline=true,
                yaxis_zeroline=true,
                xaxis_showline=true,
                yaxis_showline=true,
                xaxis_mirror=true,
                yaxis_mirror=true,
                yaxis_showgrid=false,
                xaxis_showgrid=false,
                yaxis_tickvals=collect(1:5),
                xaxis_ticks="outside",
                yaxis_ticktext=datABXSynthPosterior[:label],
                width=500, height=500)
p = plot(data, layout)
PlotlyJS.savehtml(p, string(resDir, "ABX_synth_HDIs", ".html"), :remote)


## bridge admittance figure
fHandle = MatFile("../datasets/bridge_admittance_all.mat")
specSet = get_variable(fHandle, "specSet")
freqArrSet = vec(get_variable(fHandle, "freqArrSet"))
specYamaha = get_variable(fHandle, "specYamaha")
freqArrYamaha = vec(get_variable(fHandle, "freqArrYamaha"))
close(fHandle)

minFreq = 70; maxFreq=2000
minIdxSet = findfirst(abs.(freqArrSet.-minFreq) .== minimum(abs.(freqArrSet.-minFreq)))
maxIdxSet = findfirst(abs.(freqArrSet.-maxFreq) .== minimum(abs.(freqArrSet.-maxFreq)))

minIdxY = findfirst(abs.(freqArrYamaha.-minFreq) .== minimum(abs.(freqArrYamaha.-minFreq)))
maxIdxY = findfirst(abs.(freqArrYamaha.-maxFreq) .== minimum(abs.(freqArrYamaha.-maxFreq)))

lw = 1
t1 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,1])), mode="lines", marker_color=cbPalette[6], name="Braz. Rsw.", line_width=lw)
t2 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,2])), mode="lines", marker_color=cbPalette[7], name="Ind. Rsw.", line_width=lw)
t3 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,3])), mode="lines", marker_color=cbPalette[5], name="Mahogany", line_width=lw)
t4 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,4])), mode="lines", marker_color=cbPalette[3], name="Maple", line_width=lw)
t5 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,5])), mode="lines", marker_color=cbPalette[4], name="Sapele", line_width=lw)
t6 = scatter(;x=freqArrSet[minIdxSet:maxIdxSet], y=20*log10.(abs.(specSet[minIdxSet:maxIdxSet,6])), mode="lines", marker_color=cbPalette[2], name="Walnut", line_width=lw)

tY = scatter(;x=freqArrYamaha[minIdxY:maxIdxY], y=20*log10.(abs.(specYamaha[minIdxY:maxIdxY,1])), mode="lines", marker_color="black", name="Yamaha", line_dash="dash", line_width=lw)

layout = Layout(;#xaxis_range=[log10(70), log10(5000)],
                yaxis_range=[-80, -18],
                yaxis_title="|Y| (dB re. 1 m/s/N)",
                xaxis_title="Frequency (Hz)",
                xaxis_type = "log",
                xaxis_exponentformat="none",
                font_size=16,
                hovermode="closest",
                #width=700, height=500,
                margin_l=60)
p = plot([t1, t2, t3, t4, t5, t6, tY], layout)
PlotlyJS.savehtml(p, string(resDir, "bridge_admittance", ".html"), :remote)
