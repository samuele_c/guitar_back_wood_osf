library(psychophysics) #available https://github.com/sam81/psychophysics
library(RColorBrewer)
library(R.matlab)
source("../utils.R")


##directory where to save the results
resDir = "../../results/bridge_admittance/"
dir.create(resDir, recursive=T, showWarnings=F)

gtrs = c("Rio", "Indian", "Mahog", "Maple", "Sapele", "Claro")
gtrLabels = c("Braz. Rsw.", "Indian Rsw.", "Mahogany", "Maple", "Sapele", "Walnut")

minPlotFreq = 80
maxPlotFreq = 5000

###############################
## READ BRIDGE ADMITTANCE DATA
###############################
data = readMat("../../datasets/bridge_admittance_all.mat")

minPlotIdx =  which(abs(data$freqArrSet-minPlotFreq) == min(abs(data$freqArrSet-minPlotFreq)))
maxPlotIdx =  which(abs(data$freqArrSet-maxPlotFreq) == min(abs(data$freqArrSet-maxPlotFreq)))

minPlotIdx_yamaha =  which(abs(data$freqArrYamaha-minPlotFreq) == min(abs(data$freqArrYamaha-minPlotFreq)))
maxPlotIdx_yamaha =  which(abs(data$freqArrYamaha-maxPlotFreq) == min(abs(data$freqArrYamaha-maxPlotFreq)))

minPlotIdxABX =  which(abs(data$freqArrABX-minPlotFreq) == min(abs(data$freqArrABX-minPlotFreq)))
maxPlotIdxABX =  which(abs(data$freqArrABX-maxPlotFreq) == min(abs(data$freqArrABX-maxPlotFreq)))



## JASA Figure
cbbPalette = c("#E69F00", "#56B4E9", "#009E73", "#0072B2", "#D55E00", "#CC79A7", "#000000")
cp = add.alpha(cbbPalette, 0.6)
cp = c("blue", "red", "green", "blue", "red", "green", "black")
lwd=1; ltyYamaha=3
lwdList = rep(1, 6)
ltyList = rep(c(1,2), each=3)
pdf(paste0(resDir, "bridge_admittance.pdf"), width=7.007874, height=5.2) #height=5.2
par(mar=c(0.5, 5, 1.7, 0.3), mfrow=c(2,1), oma=c(2.6,0,3.6,0), xaxs="r", ps = 12, cex = 1, cex.main = 1)

## A
plot.new(); plot.window(xlim=log10(c(minPlotFreq, maxPlotFreq)), ylim=c(-71, -20))
for (i in 1:length(gtrs)){
    gtr = gtrs[i]
    lines(log10(data$freqArrSet[minPlotIdx:maxPlotIdx]), 20*log10(abs(data$specSet[,i][minPlotIdx:maxPlotIdx])), col=cp[i], lwd=lwdList[i], lty=ltyList[i])
}

lines(log10(data$freqArrYamaha[minPlotIdx_yamaha:maxPlotIdx_yamaha]), 20*log10(abs(data$specYamaha[minPlotIdx_yamaha:maxPlotIdx_yamaha])), col=cp[7], lwd=lwd, lty=ltyYamaha)
axis(2)
logaxis(1, base=10, labels=F)
E2=82.41
axis(3, at=log10(c(E2*2^(0:6))), labels=c(expression("E"[2]), expression("E"[3]), expression("E"[4]), expression("E"[5]), expression("E"[6]), expression("E"[7]), expression("E"[8])), mgp=c(3, .7, 0))
minNoteTicks = c((E2*2^0)*2^(1:11/12), (E2*2^1)*2^(1:11/12), (E2*2^2)*2^(1:11/12), (E2*2^3)*2^(1:11/12), (E2*2^4)*2^(1:11/12), (E2*2^5)*2^(1:11/12))
axis(3, at=log10(minNoteTicks), labels=F, tcl=-0.25)
box(); 

title(ylab=expression(paste("|", italic(Y), "| (dB ", italic(re.), " 1 m/s/N)")), line=2.5)
legend(grconvertX(0.05, from = "ndc", to = "user"), grconvertY(1.02, from = "ndc", to = "user"), legend=c(gtrLabels, "Yamaha"), col=c(cp, add.alpha("black", 0.6)), bty='n', lty=c(ltyList, ltyYamaha), lwd=c(lwdList, 1), xpd=NA, ncol=3)
mtext("A", side=3, font=2, line=0.5, adj=-0.275)

## B
plot.new(); plot.window(xlim=log10(c(minPlotFreq, maxPlotFreq)), ylim=c(-71, -20))
for (i in 1:length(gtrs)){
    gtr = gtrs[i]
    if (gtr %in% c("Rio", "Indian", "Sapele", "Claro")){
        lines(log10(data$freqArrABX[minPlotIdxABX:maxPlotIdxABX]), 20*log10(abs(data$specABX[,i][minPlotIdxABX:maxPlotIdxABX])), col=cp[i], lwd=lwdList[i], lty=ltyList[i])
    }
}
lines(log10(data$freqArrABX[minPlotIdxABX:maxPlotIdxABX]), 20*log10(abs(data$specABX[,7][minPlotIdxABX:maxPlotIdxABX])), col=cp[7], lwd=lwd, lty=ltyYamaha)

axis(2)
logaxis(1, base=10, labels=F)
E2=82.41
logaxis(1, base=10, mgp=c(3, .7, 0))
axis(1, at=log10(c(300, 5000)), labels=c(300, 5000), tick=F, mgp=c(3, .7, 0))
axis(3, at=log10(c(E2*2^(0:6))), labels=c("", "", "", "", "", "", ""))
axis(3, at=log10(minNoteTicks), labels=F, tcl=-0.25)
box(); 

title(ylab=expression(paste("|", italic(Y), "| (dB ", italic(re.), " 1 m/s/N)")), line=2.5)
title(xlab="Frequency (Hz)", outer=T, line=1.5)
mtext("B", side=3, font=2, line=0.5, adj=-0.275)



dev.off() 
