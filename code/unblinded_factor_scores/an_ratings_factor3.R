t1 = Sys.time()
source("../DBDA2E-utilities_SC.R") #postprocessing utilities based on code of Kruschke
source("../DBDA2E-utilities_DF.R") #postprocessing utilities based on code of Kruschke
source("../Jags-Ymet-Xnom-SplitPlot-1Btw-1Wtn-Mrobust.R") #jags model based on code of Kruschke
currDir = getwd()
setwd("../")
source("global_parameters.R")
setwd(currDir)
source("../utils.R")
#load libraries for multi-core parallel processing
library(foreach)
library(doParallel)
library(RColorBrewer)
library(dplyr)
registerDoParallel(cores=8)

##
updateMCMCSampling = TRUE ## whether to run MCMC or use previously stored results


##directory where to save the results
resDir = "../../results/unblinded_factor_scores_F3/"
dir.create(resDir, recursive=T, showWarnings=F)
dir.create(paste0(resDir, "MCMC_diagnostics"), recursive=T, showWarnings=F)
dir.create(paste0(resDir, "figures_data"), recursive=T, showWarnings=F)
## load data
dat = read.table("../../derived_datasets/unblinded_ratings_with_EFA_scores.csv", header=T)
## set parameters for MCMC sampling
thinStepsAov = 10
numSavedStepsAov = 75000

####################################################
## set up contrasts for plots
####################################################

xBetweenContrasts = list(
    list(c("professional"), c("amateur"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("professional"), c("semi-pro"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("semi-pro"), c("amateur"), compVal=0.0, ROPE=c(-0.2,0.2))
)
xWithinContrasts = list(
    list(c("braz"), c("indi"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("braz"), c("sape"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("braz"), c("maho"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("braz"), c("waln"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("braz"), c("mapl"), compVal=0.0, ROPE=c(-0.2,0.2)),

    list(c("indi"), c("sape"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("indi"), c("maho"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("indi"), c("waln"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("indi"), c("mapl"), compVal=0.0, ROPE=c(-0.2,0.2)),

    list(c("sape"), c("maho"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("sape"), c("waln"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("sape"), c("mapl"), compVal=0.0, ROPE=c(-0.2,0.2)),

    list(c("maho"), c("waln"), compVal=0.0, ROPE=c(-0.2,0.2)),
    list(c("maho"), c("mapl"), compVal=0.0, ROPE=c(-0.2,0.2)),

    list(c("waln"), c("mapl"), compVal=0.0, ROPE=c(-0.2,0.2))
)

xBetweenWithinContrasts = list(
    list(list("professional", "amateur"), list("braz", "mapl"), compVal=0, ROPE=c(-0.2,0.2))
)

###############################
## Run MCMC sampling
###############################
if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC.RDS")) == FALSE){
    mcmcCoda = genMCMCAov1Btw1WtnRobust(datFrm=dat,
                                        yName="F3", xBetweenName="type",
                                        xWithinName="guitar", xSubjectName="subject",
                                        numSavedSteps=numSavedStepsAov, thinSteps=thinStepsAov)
    saveRDS(mcmcCoda, file=paste0(resDir, "MCMC.RDS"), compress="xz")
} else {
    mcmcCoda = readRDS(paste0(resDir, "MCMC.RDS"))
}

mcmcMat = as.matrix(mcmcCoda)

###############################
## Run MCMC diagnostics
###############################
parameterNames = varnames(mcmcCoda)
parToPlot = c("b0","bB[1]", "bB[2]", "bB[3]",
    "bW[1]", "bW[2]", "bW[3]", "bW[4]", "bW[5]", "bW[6]",
    "bS[1]", "bS[3]", "bS[6]", "bS[10]",
    "bBxW[1,1]", "bBxW[2,1]", "bBxW[3,1]",
    "bBxW[1,2]", "bBxW[2,2]", "bBxW[3,2]",
    "bBxW[1,3]", "bBxW[2,3]", "bBxW[3,3]",
    "bBxW[1,4]", "bBxW[2,4]", "bBxW[3,4]",
    "bBxW[1,5]", "bBxW[2,5]", "bBxW[3,5]",
    "bBxW[1,6]", "bBxW[2,6]", "bBxW[3,6]",
    "sigma","sigmaB","sigmaW","sigmaS","sigmaBxW",
    "mSxW[1,1]", "nu")

foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste(resDir, "MCMC_diagnostics/", parName, ".pdf", sep=""), width=7, height=5)
    diagMCMC(codaObject=mcmcCoda, parName=parName)
    dev.off()
}

## ################################
## posterior predictive checks
## red points plot results of the
## first rating session, black lines plot results of the
## second rating session
## ################################

for (i in 1:length(playerTypes)){
    pdf(paste(resDir, "post-pred-", playerTypes[i], ".pdf", sep=""), width=10)
    plotPostPredAov1Btw1WtnRobust(mcmcCoda,
                                  dat, yName="F3",
                                  xBetweenName="type", xWithinName="guitar", xSubjectName="subject",
                                  xBetweenLevel=playerTypes[i])
    dev.off()
}


pdf(paste0(resDir, "posterior_nu.pdf"))
plotPost(mcmcMat[,"nu"], ROPE=c(25, 35), compVal=30)
dev.off()

pdf(paste0(resDir, "posterior_sigma_within.pdf"))
plotPost(mcmcMat[,"sigmaW"])
dev.off()

pdf(paste0(resDir, "posterior_sigma_between.pdf"))
plotPost(mcmcMat[,"sigmaB"])
dev.off()

pdf(paste0(resDir, "posterior_sigma_betweenXwithin.pdf"))
plotPost(mcmcMat[,"sigmaBxW"])
dev.off()

pdf(paste0(resDir, "posterior_sigma_residual.pdf"))
plotPost(mcmcMat[,"sigma"])
dev.off()

## #############################################
## Get contrasts
## #############################################
cntrs = getContrastsAov1Btw1WtnRobust(mcmcCoda,
    datFrm=dat, yName="F3", xBetweenName="type",
    xWithinName="guitar", xSubjectName="subject",
    xBetweenContrasts=xBetweenContrasts,
    xWithinContrasts=xWithinContrasts,
    xBetweenWithinContrasts=xBetweenWithinContrasts)
cntrsDF = getContrastsAov1Btw1WtnRobustDF(mcmcCoda,
    datFrm=dat, yName="F3", xBetweenName="type",
    xWithinName="guitar", xSubjectName="subject",
    xBetweenContrasts=xBetweenContrasts,
    xWithinContrasts=xWithinContrasts,
    xBetweenWithinContrasts=xBetweenWithinContrasts)

## ##################################################
## Contrasts for within-subject factor (guitar wood)
## ##################################################
pdf(paste0(resDir, "posterior_within_contrasts.pdf"), width=7, height=10)
par(mfrow=c(5,3))
for (cnt in 1:length(xWithinContrasts)){
    c1 = xWithinContrasts[[cnt]][[1]]; c2 = xWithinContrasts[[cnt]][[2]]
    cName = paste(c1, "-vs-", c2, sep="")
    plotPost(cntrs[["within"]][[cName]], ROPE=c(-0.2, 0.2), compVal=0, main=cName, col=pastels1[2],
             xlab="Difference")
}
dev.off()


withinContrastsDF = data.frame()
for (i in 1:length(gPairs)){
    g1 = gPairs[[i]][1]; g2 = gPairs[[i]][2]
    thisLabelNice = paste0(gsub2(guitarCodes, guitarLabelsShort, g1), "-", gsub2(guitarCodes, guitarLabelsShort, g2))
    thisDF = summarizePostDF(cntrs[["within"]][[paste0(g1, "-vs-", g2)]], label=paste0(g1, "-vs-", g2))
    thisDF$labelNice = thisLabelNice
    withinContrastsDF = rbind(withinContrastsDF, thisDF)
}


p = ggplot(withinContrastsDF, aes(x=labelNice, y=Mode)) + geom_point(shape=1)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + scale_y_continuous(limits=c(-1,1))
p = p + theme_bw2 + theme(axis.title.x = element_text(hjust=1)) 
p = p + ylab("Factor 3 Score Difference") + theme(axis.title.x = element_text(hjust=1)) + xlab(NULL)
ggsave(filename=paste0(resDir, "HDIs_contrasts_within.pdf"), width=3.4252, height=3.4252)
write.table(withinContrastsDF, paste0(resDir, "figures_data/df_within_contrasts.csv"), row.names=F, col.names=T)


## ##################################################
## Contrasts for between-subject factor (player type)
## ##################################################
betweenContrastsDF = data.frame()

for (i in 1:length(pTypePairs)){
    g1 = pTypePairs[[i]][1]; g2 = pTypePairs[[i]][2]
    thisLabelNice = paste0(gsub2(playerTypes, playerLabelsShort, g1), "-", gsub2(playerTypes, playerLabelsShort, g2))
    thisDF = summarizePostDF(cntrs[["between"]][[paste0(g1, "-vs-", g2)]], label=paste0(g1, "-vs-", g2))
    thisDF$labelNice = thisLabelNice
    betweenContrastsDF = rbind(betweenContrastsDF, thisDF)
}
betweenContrastsDF$labelNice = factor(betweenContrastsDF$labelNice, levels=c("Pro-Semipro", "Pro-Amateur", "Semipro-Amateur"), labels=c("Pro-Semipro", "Pro-Amateur", "Semipro-Amateur"))

p = ggplot(betweenContrastsDF, aes(x=labelNice, y=Mode)) + geom_point(shape=1)
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + scale_y_continuous(limits=c(-1,1))
p = p + theme_bw2
p = p + ylab("Factor 3 Score Difference") + theme(axis.title.x = element_text(hjust=1)) + xlab(NULL)
ggsave(filename=paste0(resDir, "HDIs_contrasts_between.pdf"), width=3.4252, height=1.5)
write.table(betweenContrastsDF, paste0(resDir, "figures_data/df_between_contrasts.csv"), row.names=F, col.names=T)

pdf(paste0(resDir, "posterior_contrasts_between.pdf"), width=7, height=6)
par(mfrow=c(1,2))
for (cnt in 1:length(xBetweenContrasts)){
    c1 = xBetweenContrasts[[cnt]][[1]]; c2 = xBetweenContrasts[[cnt]][[2]]
    cName = paste(c1, "-vs-", c2, sep="")
    plotPost(cntrs[["between"]][[cName]], ROPE=c(-0.2, 0.2), compVal=0, main=cName, col=pastels1[2],
             xlab="Difference")
}
dev.off()

## ###############################################
## Contrasts between guitars for each player type
## ###############################################

mcByType = list()
for (pType in playerTypes){
    mcByType[[pType]] = list()
    for (gtrCode in guitarCodes){
        typeN = findLevNum(pType, dat$type)
        guitN = findLevNum(gtrCode, dat$guitar)
        mcByType[[pType]][[gtrCode]] = mcmcMat[,"b0"] + mcmcMat[,paste0("bB[", typeN, "]")] + mcmcMat[,paste0("bW[", guitN, "]")] + mcmcMat[,paste0("bBxW[", typeN, ",", guitN, "]")]
    }
}

withinContrastsDFByType = data.frame()
for (pType in playerTypes){
    for (i in 1:length(gPairs)){
        g1 = gPairs[[i]][1]; g2 = gPairs[[i]][2]
        thisLabelNice = paste0(gsub2(guitarCodes, guitarLabelsShort, g1), "-", gsub2(guitarCodes, guitarLabelsShort, g2))
        thisDF = summarizePostDF(mcByType[[pType]][[g1]] - mcByType[[pType]][[g2]], label=paste0(g1, "-vs-", g2))
        thisDF$type = pType
        thisDF$labelNice = thisLabelNice
        withinContrastsDFByType = rbind(withinContrastsDFByType, thisDF)
    }

}

withinContrastsDFByType$typeLabel = factor(withinContrastsDFByType$type, levels=c("professional", "semi-pro", "amateur"), labels=c("Professional", "Semi-pro", "Amateur"), ordered=T)
dodgeSize = -0.75
p = ggplot(withinContrastsDFByType, aes(x=labelNice, y=Mode, shape=typeLabel, color=typeLabel)) + geom_point(position=position_dodge(dodgeSize))
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0, position=position_dodge(dodgeSize))
p = p + geom_hline(yintercept=0, linetype=3) + coord_flip()
p = p + scale_y_continuous(limits=c(-2,2))
p = p + theme_bw2 + theme(legend.position=c(0.2, 1.07),
    legend.direction="horizontal",
    plot.margin=unit(c(1.5,0.2,0.2,0.2),"lines"))
p = p + ylab("Factor 3 Score Difference") + theme(axis.title.x = element_text(hjust=1)) + xlab(NULL)
p = p + scale_shape_discrete(name=NULL)
p = p + scale_color_manual(name=NULL, values=pTypeCol)
ggsave(filename=paste0(resDir, "HDIs_contrasts_within_by_type.pdf"), width=3.4252, height=3.8)
write.table(withinContrastsDFByType, paste0(resDir, "figures_data/df_within_contrasts_by_type.csv"), row.names=F, col.names=T)

for (pType in playerTypes){
    pdf(paste0(resDir, "within_contrasts_", pType, ".pdf"), width=7, height=10)
    par(mfrow=c(5,3))
    for (i in 1:length(gPairs)){
        p1 = gPairs[[i]][1];  p2 = gPairs[[i]][2];
        plotPost(mcByType[[pType]][[g1]] - mcByType[[pType]][[g2]], ROPE=c(-0.2, 0.2), compVal=0, main=paste(p1, "-", p2) , col=pastels1[2], xlab="Difference")
    }
    dev.off()
}


t2 = Sys.time()
print(t2-t1)


