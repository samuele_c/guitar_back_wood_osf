t1 = Sys.time()
source("../DBDA2E-utilities_SC.R") #postprocessing utilities based on code of Kruschke
source("../DBDA2E-utilities_DF.R") #postprocessing utilities based on code of Kruschke
source("../Jags-Ymet-Xnom-1Fac-Wtn-Mrobust.R") #jags model based on code of Kruschke
currDir = getwd()
setwd("../")
source("global_parameters.R")
setwd(currDir)
source("../utils.R")
#load libraries for multi-core parallel processing
library(foreach)
library(doParallel)
library(RColorBrewer)
registerDoParallel(cores=8)


##
updateMCMCSampling = TRUE ## whether to run MCMC or use previously stored results

##directory where to save the results
resDir = "../../results/ABX_synth/"
dir.create(resDir, recursive=T, showWarnings=F)
dir.create(paste0(resDir, "MCMC_diagnostics"), recursive=T, showWarnings=F)
dir.create(paste0(resDir, "figures_data"), recursive=T, showWarnings=F)

## set parameters for MCMC sampling
numSavedStepsAov = 75000
thinStepsAov = 10

datABXSynth = read.table("../../derived_datasets/ABX_synth_dprime.csv", header=T)
datABXSynth$condition = factor(datABXSynth$condition, levels=c("Braz_Rsw_vs_Sapele", "Braz_Rsw_vs_Walnut", "Sapele_vs_Walnut", "Ind_Rsw_vs_Sapele",  "Ind_Rsw_vs_Yamaha"))
datABXSynth$conditionLabels = factor(datABXSynth$conditionLabels, c("Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut", "Ind. Rsw. vs Sapele", "Ind. Rsw. vs Yamaha"))
##L4:semi-pro
##L5: pro
##L7: pro
##L16: semi-pro
##L18: semi-pro
##L23: pro
##L36: semi-pro

##clsAllListeners = adjustcolor(colorBlindPalette(), alpha.f=0.6)
clsAllListeners = adjustcolor(brewer.pal(8, "Dark2"), alpha.f=0.6)
cls4 = c("black", "slateblue")
singCol = brewer.pal(3, "Pastel2")[1]#"gray75"

if (updateMCMCSampling == TRUE | file.exists(paste0(resDir, "MCMC.RDS")) == FALSE){
    mcmcCoda = genMCMCAovOnewayWtnRobust(datABXSynth, "dprime", "conditionLabels", "listener", numSavedSteps=10000, thinSteps=10) 
    saveRDS(mcmcCoda, file=paste0(resDir, "MCMC.RDS"), compress="xz")
} else {
    mcmcCoda = readRDS(paste0(resDir, "MCMC.RDS"))
}



parameterNames = varnames(mcmcCoda) # get all parameter names
## parToPlot = c("b0", "bW[1]", "bW[2]", "bW[3]", "bW[4]", "bW[5]",
##     "bS[1]", "bS[2]", "bS[3]", "bS[4]", "nu", "sigma", "sigmaW", "sigmaS")
parToPlot = parameterNames

foreach (p=1:length(parToPlot)) %dopar% {
    parName = parToPlot[p]
    pdf(file=paste0(resDir, "MCMC_diagnostics/", parName, ".pdf"), width=7, height=5)
    diagMCMC(codaObject=mcmcCoda, parName=parName)
    dev.off()
}


mcmcMat = as.matrix(mcmcCoda, chains=T)

pdf(paste0(resDir, "posterior_nu.pdf"))
plotPost(mcmcMat[,"nu"], ROPE=c(25, 35), compVal=30)
dev.off()

pdf(paste0(resDir, "FullByCnd.pdf"))
par(mfrow=c(2,2))
cnd1 = plotPost(mcmcMat[,'b0']+mcmcMat[,'bW[1]'], main='Braz. Rosewood-Sapele', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
cnd2 = plotPost(mcmcMat[,'b0']+mcmcMat[,'bW[2]'], main='Braz. Rosewood-Walnut', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
cnd3 = plotPost(mcmcMat[,'b0']+mcmcMat[,'bW[3]'], main='Sapele-Walnut', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
cnd4 = plotPost(mcmcMat[,'b0']+mcmcMat[,'bW[4]'], main='Ind Rsw-Sapele', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
cnd5 = plotPost(mcmcMat[,'b0']+mcmcMat[,'bW[5]'], main='Ind. Rsw - Yamaha', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
dev.off()

pdf(paste0(resDir, "posterior_b0.pdf"))
plotPost(mcmcMat[,'b0'], main="Average d'", cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
dev.off()

##contrasts with Ind. Rsw. - Yamaha
pdf(paste0(resDir, "contrasts_Ind_Rsw_Yamaha.pdf"))
c1 = plotPost(mcmcMat[,'bW[5]']-mcmcMat[,'bW[1]'], main='Ind. Rsw - Yamaha vs Braz. Rosewood-Sapele', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
c2 = plotPost(mcmcMat[,'bW[5]']-mcmcMat[,'bW[2]'], main='Ind. Rsw - Yamaha vs Braz. Rosewood-Walnut', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
c3 = plotPost(mcmcMat[,'bW[5]']-mcmcMat[,'bW[3]'], main='Ind. Rsw - Yamaha vs Braz. Sapele-Walnut', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
c4 = plotPost(mcmcMat[,'bW[5]']-mcmcMat[,'bW[4]'], main='Ind. Rsw - Yamaha vs Braz. Ind. Rsw. -Sapele', cenTend="mode", compVal=0, ROPE=c(-0.2, 0.2))
dev.off()

nSamp = length(mcmcMat[,'b0'])
mcVals = c(mcmcMat[,'b0']+mcmcMat[,'bW[1]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[2]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[3]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[4]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[5]'])
mcLabs = c(rep(findLevLab(1, datABXSynth$conditionLabels), nSamp),
    rep(findLevLab(2, datABXSynth$conditionLabels), nSamp),
    rep(findLevLab(3, datABXSynth$conditionLabels), nSamp),
    rep(findLevLab(4, datABXSynth$conditionLabels), nSamp),
    rep(findLevLab(5, datABXSynth$conditionLabels), nSamp))

mcFrame = data.frame(val=mcVals, label=mcLabs)
mcFrame$label = factor(mcFrame$label,
    levels=c("Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut", "Ind. Rsw. vs Sapele",  "Ind. Rsw. vs Yamaha"),
    labels=c("Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut", "Ind. Rsw. vs Sapele",  "Ind. Rsw. vs Yamaha"))


prmLabels = c(findLevLab(1, datABXSynth$conditionLabels),
    findLevLab(2, datABXSynth$conditionLabels),
    findLevLab(3, datABXSynth$conditionLabels),
    findLevLab(4, datABXSynth$conditionLabels),
    findLevLab(5, datABXSynth$conditionLabels)) 
prmChains = list(mcmcMat[,'b0']+mcmcMat[,'bW[1]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[2]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[3]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[4]'],
    mcmcMat[,'b0']+mcmcMat[,'bW[5]'])

summFrame = summarizePostListDF(prmChains, labels=prmLabels)
summFrame$label = factor(summFrame$label,
    levels=c("Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut", "Ind. Rsw. vs Sapele",  "Ind. Rsw. vs Yamaha"),
    labels=c("Braz. Rsw. vs Sapele", "Braz. Rsw. vs Walnut", "Sapele vs Walnut", "Ind. Rsw. vs Sapele",  "Ind. Rsw. vs Yamaha"))

write.table(summFrame, file=paste0(resDir, "figures_data/summDF.csv"), col.names=T, row.names=F)

p = ggplot(summFrame, aes(x=label, y=Mode)) 
p = p + geom_errorbar(aes(ymin=HDIlow, ymax=HDIhigh), width=0)
p = p + geom_point(size=2, shape=16, col=singCol)
p = p + geom_point(size=2, shape=1)
p = p + geom_hline(yintercept=0, linetype=2) + coord_flip()
p = p + xlab(NULL) + ylab(dpText) + scale_y_continuous(limits=c(0,4))
p = p + theme_bw2
p_posterior = p
ggsave(paste0(resDir, "posterior.pdf"), p, width=3.307, height=2.307)


